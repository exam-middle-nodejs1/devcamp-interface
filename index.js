// import express
const express = require('express');
//import thu vien path
const path = require("path")

//import mongoose 
const mongoose = require('mongoose')

//khoi tao express
const app = express();

// cau hinh de app doc dc body
app.use(express.json());

// cau hinh de app dc dc UTF8
app.use(express.urlencoded({
    extended: true
}));

//khai bao cong cua project
const port = 8000;

app.use(express.static(__dirname +`/views`))

//khai bao mongoose
mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (err) => {
    if (err) {
        throw err;
    }
    console.log("Connect MongoDB Successfully!");
})

// Cấu hình để app đọc được body request dạng json
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
    (request, response, next) => {
        console.log("Request method: ", request.method);
        next();
    })

// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get ("/interface", (request, response ) => {
    console.log (__dirname);

    response.sendFile(path.join(__dirname +'/views/interface.html'));
})

app.get("/interface", (request, response) => {
    let today = new Date();
    response.status(200).json({
        message: `Hôm này là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`

    })
});

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})
